----
--  RemoteEmote
--  @Description ~%description%~
--  @Author ~%author%~
--  @Version ~%version%~
--  @Patch ~%patch%~
--

require "lib/lib_ChatLib";
require "lib/lib_Slash";
require "lib/lib_InterfaceOptions";
require "lib/lib_HudNote";

require "string";
require "math";

require "./lib/lib_CustomChatLink";
require "./emotelist";
require "./help";
 

require "lib/lib_Debug";

--~~~~~~~~~~~~~--
-- Constants   --
--~~~~~~~~~~~~~--

local MSG = nil;
local channels = {
  "say",
  "yell",
  "zone",
  "zone_lang",
  "community",
  "squad",
  "platoon",
  "team",
  "army",
  "officer",
  "whisper",
  "lobby"
}

local NOTES = {     
  ["askWhitelist"] = {
    title = "RemoteEmote",
    subtitle = "Whitelist request",
    p1 = "Allow",
    p2 = "Ignore"
  }
}

local g_info = {}
local g_whitelist = {}
local g_blacklist = {}
local g_hudnotes = {}

--~~~~~~~~~~~~~--
-- Options     --
--~~~~~~~~~~~~~--

local ADDON = {
  NAME = "RemoteEmote",
  VERSION = "~%version%~",
  AUTHOR = "~%author%~",
  LOADED = false,
  DEBUG = '~%debug%~'
}

local OPTIONS = {
  enabled = true,
  channel = channels[1],
  targets = "",
  whitelist_request_duration = 5,
  whitelist_perm = "",
  blacklist_request_duration = 5,
  blacklist_perm = "",
  emote_delay = 1000
}

OnMessageSetData = {
  enabled = function(message)
    
    OPTIONS.enabled = message
    
    if(ADDON.LOADED and message) then
      --re-enable stuff
      init();
    else
      --shut down
      if(ADDON.LOADED) then
        shutdown();
      end
    end
  end,
  
  channel = function(message)
    
    OPTIONS.channel = channels[message]
    
  end,
  
  targets = function(message)
    OPTIONS.targets = message
  end,
  
  whitelist_request_duration = function(message)
    OPTIONS.whitelist_request_duration = message
  end,
  
  whitelist_perm = function(message)
    OPTIONS.whitelist_perm = message
  end,
  
  blacklist_request_duration = function(message)
    OPTIONS.blacklist_request_duration = message
  end,
  
  blacklist_perm = function(message)
    OPTIONS.blacklist_perm = message
  end,
  
  emote_delay = function(message)
    OPTIONS.emote_delay = message
  end,
  
  clear_whitelist = function(message)
    g_whitelist = {}
  end,
  
  clear_blacklist = function(message)
    g_blacklist = {}
  end
  
}

InterfaceOptions.SaveVersion(1)
InterfaceOptions.StartGroup({id="enabled", label="Enable "..ADDON.NAME, checkbox=true, default=OPTIONS.enabled, tooltip="Disables the Addon if unchecked"});
  InterfaceOptions.AddChoiceMenu({id="channel", label="Channel", default=1, tooltip="Which channel to use when sending emotes"});
  for k, v in pairs(channels) do
    InterfaceOptions.AddChoiceEntry({menuId="channel", label=v, val=k})
  end
  InterfaceOptions.AddTextInput({id="targets", label="Whisper Targets", default=OPTIONS.targets, tooltip="Which players to whisper if channel is set accordingly\nSeperate names with a comma (,)"});
  InterfaceOptions.AddSlider({id="whitelist_request_duration", label="Whitelist Request Duration", default=OPTIONS.whitelist_request_duration, min=5, max=60, inc=5, suffix=" min", tooltip="How long your whitelist requests should be valid"});
  InterfaceOptions.AddTextInput({id="whitelist_perm", label="Permanent Whitelist", default=OPTIONS.whitelist_perm, tooltip="A list of players that are always allowed to send emotes\nSeperate names with a comma (,)"});
  InterfaceOptions.AddSlider({id="blacklist_request_duration", label="Blacklist Request Duration", default=OPTIONS.blacklist_request_duration, min=1, max=30, inc=1, suffix=" min", tooltip="How long to automatically ignore users when declining requests"});
  InterfaceOptions.AddTextInput({id="blacklist_perm", label="Permanent Blacklist", default=OPTIONS.blacklist_perm, tooltip="A list of players that are blocked permanentlys\nSeperate names with a comma (,)"});
  InterfaceOptions.AddSlider({id="emote_delay", label="Emote Delay", default=OPTIONS.emote_delay, min=0, max=10000, inc=50, suffix=" ms", tooltip="How far ahead the emote should be planned, to compensate for communication delays"});
  InterfaceOptions.AddButton({id="clear_whitelist", label="Clear Temporary Whitelist", tooltip="Reset all accepted users"})
  InterfaceOptions.AddButton({id="clear_blacklist", label="Clear Temporary Blacklist", tooltip="Reset all blocked users"})
  
InterfaceOptions.StopGroup()
InterfaceOptions.NotifyOnLoaded(true)

function OnMessage(msg)
  if msg.type == "__LOADED" then
    ADDON.LOADED = true
    init();
  end
  if not ( OnMessageSetData[msg.type] ) then return nil end
  OnMessageSetData[msg.type](msg.data);
end

--~~~~~~~~~~~~~--
-- Collections --
--~~~~~~~~~~~~~--

local CHAT = {
  requestWhitelist = function()
    
    local value = {
      action="askWhitelist",
      options={
        whitelist_request_duration = OPTIONS.whitelist_request_duration,
        end_time = tonumber(System.GetClientTime()) + OPTIONS.whitelist_request_duration * 60 * 1000
      }
    }
  
    sendMessage(value);
    
  end,
  
  sendEmote = function(emote)
    
    local value = {
      action="runEmote",
      options={
        emote = emote,
        start_time = tonumber(System.GetClientTime()) + OPTIONS.emote_delay
      }
    }
    
    sendMessage(value);
    
  end
  
  
}

local CHAT_ACTIONS = {

  askWhitelist = function(args, options)
  
      local sname = ChatLib.StripArmyTag(args.author)
      local splayer = ChatLib.StripArmyTag(g_info.name)
      
      if(sname ~= splayer)then
        if(g_hudnotes[args.author] == nil and not(isBlacklisted(args)) and not(isWhitelisted(args)))then
          createHudNote("askWhitelist", args, options)
        end
    
    end
  end,

  runEmote = function(args, options)
    if(isWhitelisted(args) and not(isBlacklisted(args)))then
      if(emotes[options.emote])then
        
        local currentTime = tonumber(System.GetClientTime());
        local plannedTime = options.start_time
        
        local delay = plannedTime - currentTime;
        if(delay < 0) then delay = 0 end
        
        callback(Game.SlashCommand, options.emote, delay/1000);
      end
    end
  end
}

local ACTIONS = {

  allowWhitelist = function(t)
    Debug.Log("allow")  
    t.HUDNOTE:Remove();
    g_hudnotes[t.args.author] = nil;
    local author = ChatLib.StripArmyTag(t.args.author);
    g_whitelist[author] = t.options;
  end,
  
  declineWhitelist = function(t)
    Debug.Log("decline")
    t.HUDNOTE:Remove();
    g_hudnotes[t.args.author] = nil;
    t.options.end_time = tonumber(System.GetClientTime()) + OPTIONS.blacklist_request_duration * 60 * 1000;
    local author = ChatLib.StripArmyTag(t.args.author);
    g_blacklist[author] = t.options;
  end

}

NOTES["askWhitelist"].f1 = ACTIONS.allowWhitelist;
NOTES["askWhitelist"].f2 = ACTIONS.declineWhitelist;

--~~~~~~~~~~~~~--
-- Events      --
--~~~~~~~~~~~~~--

-- XML

function OnComponentLoad() 

  log(ADDON.NAME..": --- version " .. ADDON.VERSION ..  " ---")
  local debug = (ADDON.DEBUG == '~%debug%~' or ADDON.DEBUG == 'true')
  Debug.EnableLogging(debug);
    
  InterfaceOptions.SetCallbackFunc(function(id, val)  OnMessage({type=id, data=val})  end, ADDON.NAME.." "..ADDON.VERSION)
  
end

-- LUA

function OnChatPacket(args)
    Debug.Log(args);
    if(OPTIONS.enabled)then
      local value = jsontotable(args.link_data);
      CHAT_ACTIONS[value.action](args, value.options);
    end
end


--~~~~~~~~~~~~~--
-- Functions   --
--~~~~~~~~~~~~~--

-- general
function init()
  g_info = Game.GetTargetInfo(Player.GetTargetId())
  if(OPTIONS.enabled)then
    LIB_SLASH.BindCallback({slash_list = 're', description = '', func = executeSlashCommand});
    if(MSG == nil)then
      MSG = CustomChatLink.new("RemoteEmote", OnChatPacket);
    end
  end
end

function shutdown()
  LIB_SLASH.UnbindCallback('re');
end

function systemMessage(args)
  Component.GenerateEvent("MY_SYSTEM_MESSAGE", {text=tostring(args)})
end

function tablelength(T)
  local count = 0
  for _ in pairs(T) do count = count + 1 end
  return count
end

function round(val, decimal)
  local exp = decimal and 10^decimal or 1
  return math.ceil(val * exp - 0.5) / exp
end

function calculateMinutes(ts)

  local currentTime = tonumber(System.GetClientTime());
  local remaining = (ts - currentTime)/1000/60;
  
  local string = "";
  string = string .. tostring(math.floor(remaining)) .. ":"; 
  local seconds = round(60 * (remaining % 1), 0);
  if(seconds < 10)then
    seconds = "0"..tostring(seconds);
  end
  string = string .. seconds;
  
  return string;
end


function executeSlashCommand(args)
  Debug.Log(args);
  
  -- /re auth
  if(args[1] == "auth")then
    CHAT.requestWhitelist();
    return;
  end
  
  -- /re em <emote>
  if(args[1] == "em" and args[2] ~= nil)then
    if(emotes[args[2]])then
      CHAT.sendEmote(args[2]);
    end
    return;
  end
  
  -- /re wl, /re whitelist
  if((args[1] == "wl" or args[1] == "whitelist") and args[2] ~= nil)then
    
    local author = ChatLib.StripArmyTag(args[2]);
    local factor = 60 * 1000;
    local duration = tonumber(System.GetClientTime());
    
    
    if(args[3] ~= nil)then
       duration = duration + tonumber(args[3]) * factor;  
    else
      duration = duration + OPTIONS.whitelist_request_duration * factor;
    end
    
    g_whitelist[author] = {end_time = duration};
    
    return;
  end
  
  -- /re bl, /re blacklist
  if((args[1] == "bl" or args[1] == "blacklist") and args[2] ~= nil)then
    
    local author = ChatLib.StripArmyTag(args[2]);
    local factor = 60 * 1000;
    local duration = tonumber(System.GetClientTime());
    
    if(args[3] ~= nil)then
       duration = duration + tonumber(args[3]) * factor;  
    else
      duration = duration + OPTIONS.blacklist_request_duration * factor;
    end
    
    g_blacklist[author] = {end_time = duration};
    
    return;
  end
  
  -- /re list wl, ...
  if((args[1] == "list" or args[1] == "show") and args[2] ~= nil)then
    
    if((args[2] == "wl" or args[2] == "whitelist"))then
      if(tablelength(g_whitelist) > 0)then
        systemMessage("<RemoteEmote> Temporary Whitelist:")
        for k, v in pairs(g_whitelist)do
          systemMessage("  "..k.." ("..calculateMinutes(v.end_time).." min)");  
        end
      else
        systemMessage("<RemoteEmote> No whitelist entries")
      end
    end
    
    if((args[2] == "bl" or args[2] == "blacklist"))then
      if(tablelength(g_blacklist) > 0)then
        systemMessage("<RemoteEmote> Temporary Blacklist:")
        for k, v in pairs(g_blacklist)do
          systemMessage("  "..k.." ("..calculateMinutes(v.end_time).." min)");  
        end
      else
        systemMessage("<RemoteEmote> No blacklist entries")
      end
    end
    
    return;
  end
  
  --/re clear wl, ...
  if((args[1] == "clear" or args[1] == "reset") and args[2] ~= nil)then
    
    if((args[2] == "wl" or args[2] == "whitelist"))then
      g_whitelist = {}
      systemMessage("<RemoteEmote> Whitelist reset")
    end
    
    if((args[2] == "bl" or args[2] == "blacklist"))then
      g_blacklist = {}
      systemMessage("<RemoteEmote> Blacklist reset")
    end
    
    return;
  end
  
  -- /re, /re help
  if(not(args[1]) or (args[1]) == "help")then
    
    for k, v in ipairs(help) do
      systemMessage(v);
    end
    
    return;
  end
  
end

function sendMessage(value)
  MSG:setValue(tostring(value));
  if(OPTIONS.channel == "whisper")then
    MSG:setWhisper(true);
    for name in string.gmatch(OPTIONS.targets, '([^,%s*]+)') do

        MSG:setTarget(name);
        MSG:sendLink();

    end
  else
    MSG:setWhisper(false);      
    MSG:setTarget(OPTIONS.channel);
    MSG:sendLink();  
  end
end

-- white/blacklist

function isWhitelisted(args)
    
    local author = ChatLib.StripArmyTag(args.author);
    for name in string.gmatch(OPTIONS.whitelist_perm, '([^,%s*]+)') do
        name = ChatLib.StripArmyTag(name);
        if(name == author)then
          return true
        end
    end
  
  for k, v in pairs(g_whitelist)do
    local currentTime = tonumber(System.GetClientTime());
    if(v.end_time - currentTime < 0)then
      g_whitelist[k] = nil;
    end
  end
  
  return ((g_info.name == args.author) or (g_whitelist[author] ~= nil));
  
end

function isBlacklisted(args)
  
    local author = ChatLib.StripArmyTag(args.author);
    for name in string.gmatch(OPTIONS.blacklist_perm, '([^,%s*]+)') do
        name = ChatLib.StripArmyTag(name);
        if(name == author)then
          return true
        end
    end
  
  for k, v in pairs(g_blacklist)do
    local currentTime = tonumber(System.GetClientTime());
    if(v.end_time - currentTime < 0)then
      g_blacklist[k] = nil;
    end
  end
  
  return (g_blacklist[author] ~= nil);
  
end

-- hudnotes

function createHudNote(type, args, options)

  local HUDNOTE = HudNote.Create()
  HUDNOTE:SetTitle(NOTES[type].title, NOTES[type].subtitle)
  
  local desc = args.author.." is asking to be whitelisted for "..options.whitelist_request_duration.." minutes"

  HUDNOTE:SetDescription(desc)
  HUDNOTE:SetIconTexture("icons", "com_tower")
  HUDNOTE:SetPrompt(1, NOTES[type].p1, NOTES[type].f1, {HUDNOTE = HUDNOTE, args = args, options = options})
  HUDNOTE:SetPrompt(2, NOTES[type].p2, NOTES[type].f2, {HUDNOTE = HUDNOTE, args = args, options = options})
  HUDNOTE:SetTimeout(20, function() HUDNOTE:Remove() end)
  
  HUDNOTE:Post()
  
  g_hudnotes[args.author] = {HUDNOTE = HUDNOTE, args = args, options = options};
end






