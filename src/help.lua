help = {
  "<RemoteEmote> Command Help:",
  
  "/re auth",
  "  broadcasts a request to be whitelisted as set in the options",
  
  "/re em <emote>",
  "  broadcasts the specified emote to everyone that has you on their whitelist",
  
  "/re wl <player>, /re whitelist <player>",
  "  manually add a player to your whitelist for the default duration (options)",
  
  "/re wl <player> <minutes>, /re whitelist <player> <minutes>",
  "  manually whitelists a player for the specified duration",
  
  "/re bl <player>, /re blacklist <player>",
  "/re bl <player> <minutes>,  /re blacklist <player> <minutes>",
  "  same as above, but adds them to your blacklist",
  
  "/re list wl, /re show blacklist, ... (same aliases as before)",
  "  shows the current entries in your white/blacklists",
  
  "/re clear wl, /re reset blacklist, ... (same aliases as before)",
  "  clears the specified temporary list"
}