----
--  CustomChatLink Library
--  @Author Jotaro
--  @Version 1.0.0
--

--[===[
  
  Usage:
    
    local MSG = CustomChatLink.new("uniqueIdentifier", callback);
      callback will receive the same args as defined in lib_ChatLib on ChatLib.RegisterCustomLinkType
    
    MSG:setWhisper(isWhisper)
      true or false, defaults to true
      true: whispers the target as specified below
      false: uses target chat channel
    
    MSG:setTarget(target)
      a player name or a chat channel, depending on the setting above
      defaults to the player's own name
    
    MSG:setValue(value)
      the data to be sent with the link
      default is a blank string
    
    MSG:sendLink();
      sends the message 
  
--]===]

if CustomChatLink then
  return nil
end
CustomChatLink = {}

require "lib/lib_ChatLib";

local API = {}
local LINK = {
  TYPE = "Message",
  END = ChatLib.GetEndcapString(),
  BREAK = ChatLib.GetLinkTypeIdBreak(),
}

local OPTIONS = {
  WHISPER = true,
  TARGET = "",
  VALUE = ""
}

local MESSAGE_METATABLE = {
  __index = function(t,key) return API[key]; end,
  __newindex = function(t,k,v) error("cannot write to value '"..k.."' in message"); end
};

function CustomChatLink.new(type, callback)
  local MSG = {
    LINK = LINK,
    OPTIONS = OPTIONS,
    CALLBACK = callback
  };
  
  LINK.TYPE = tostring(type);
  local tinfo = Game.GetTargetInfo(Player.GetTargetId())
  OPTIONS.TARGET = ChatLib.StripArmyTag(tinfo.name);
  
  
  ChatLib.RegisterCustomLinkType(type, callback);
  setmetatable(MSG, MESSAGE_METATABLE);
  return MSG;
end

-- getters & setters

function API:setWhisper(whisper)
  self.OPTIONS.WHISPER = (whisper);
end

function API:getWhisper()
  return self.OPTIONS.WHISPER;
end

function API:setTarget(target)
  self.OPTIONS.TARGET = (target);
end

function API:getTarget()
  return self.OPTIONS.TARGET;
end

function API:setValue(value)
  self.OPTIONS.VALUE = (value);
end

function API:getValue()
  return self.OPTIONS.VALUE;
end

-- api

function API:makeLink()
  return self.LINK.END..self.LINK.TYPE..self.LINK.BREAK..tostring(self.OPTIONS.VALUE)..self.LINK.END;
end


function API:sendLink()
  if(self.OPTIONS.WHISPER)then
    Chat.SendWhisperText(self.OPTIONS.TARGET, self:makeLink());
  else
    Chat.SendChannelText(self.OPTIONS.TARGET, self:makeLink());
  end

  
end