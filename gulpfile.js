var path = require('path');
var gulp = require('gulp');
var file = require('gulp-file');
var replace = require('gulp-replace');
var zip = require('gulp-zip');
var del = require('del');


var p = require('./package.json')

var root = './src'; // without trailing slash
var releasePath = '../../_releases';

gulp.task('files', function(){
  var files = gulp.src([
    root + '/**/*.lua',
    root + '/**/*.xml'
  ])
  return files
    .pipe(replace(/~%author%~/g, p.author))
    .pipe(replace(/~%version%~/g, p.version))
    .pipe(replace(/~%description%~/g, p.description))
    .pipe(replace(/~%patch%~/g, p.patch))
    .pipe(replace(/~%debug%~/g, 'false'))
    .pipe(gulp.dest('./build/' + p.name))
});

gulp.task('melder', function() {
  var br = "\n";
  
  var str = "title=" + p.name + br +
            "author=" + p.author + br +
            "version=" + p.version + br +
            "patch=" + p.patch + br +
            "url=" + p.url + br +
            "destination=" + p.destination + br +
            "description=" + p.description;
    
  return file('melder_info.ini', str, { src: true }).pipe(gulp.dest('./build'));
});

gulp.task('zip', ['melder', 'files'], function () {
    return gulp.src('build/**/*.*')
        .pipe(zip(p.name + '_' + p.version + '.zip'))
        .pipe(gulp.dest('dist'));
});

gulp.task('clean:build', ['zip'], function (cb) {
  return del([
    './build/**'
  ], cb);
});

gulp.task('clean:dist', function (cb) {
  return del([
    './dist/**'
  ], cb);
});

gulp.task('release', ['build'] , function(){
  var rel = gulp.src('dist/*.zip')
  return rel.pipe(gulp.dest(releasePath))
});

// defining all task dependencies

gulp.task('copy', ['melder', 'files'])
gulp.task('build', ['clean:dist', 'copy', 'zip', 'clean:build'])
gulp.task('default', ['build', 'release'])



